import {
  Controller,
  Post,
  UseGuards,
  Get,
  Body,
  Req,
  Res,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public, ResponseMessage, User, IUser } from '../utils';
import { LocalAuthGuard } from './passport/local-auth.guard';
import { RegisterUserDto } from '../users/dto/create-user.dto';
import { Request, Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @ResponseMessage('Login a user')
  @Post('login')
  async handleLogin(
    @Req() req,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.authService.login(req.user, response);
  }

  @Public()
  @ResponseMessage('Register a new user')
  @Post('register')
  handleRegister(@Body() registerUserDto: RegisterUserDto) {
    return this.authService.register(registerUserDto);
  }

  @ResponseMessage('Get user information')
  @Get('account')
  async handleGetAccount(@User() user: IUser) {
    return {
      ...user,
      permissions: await this.authService.getUserPermissions(user._id),
    };
  }

  @Public()
  @ResponseMessage('Get user by refresh token')
  @Get('refresh')
  handleRefreshToken(
    @Req() req: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.authService.processRefreshToken(
      req.cookies['refresh_token'],
      response,
    );
  }

  @ResponseMessage('Logout User')
  @Post('logout')
  handleLogout(
    @Req() req: Request,
    @Res({ passthrough: true }) response: Response,
    @User() user: IUser,
  ) {
    return this.authService.logout(
      req.cookies['refresh_token'],
      response,
      user,
    );
  }
}
