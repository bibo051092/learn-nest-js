import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { checkPassword, ROLE_USER, IUser, IGetUser } from '../utils';
import { RegisterUserDto } from '../users/dto/create-user.dto';
import { ConfigService } from '@nestjs/config';
import ms from 'ms';
import { Response } from 'express';
import mongoose from 'mongoose';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async validateUser(username: string, pass: string) {
    const user = await this.usersService.findByEmail(username);

    if (!user) {
      return null;
    }

    if (!checkPassword(pass, user.password)) {
      throw new UnauthorizedException();
    }

    return user;
  }

  async login(user: IUser, response: Response) {
    return this.handleToken(user, response);
  }

  async register(registerUserDto: RegisterUserDto) {
    return await this.usersService.create(registerUserDto, undefined);
  }

  createRefreshToken(payload: object) {
    return this.jwtService.sign(payload, {
      secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      expiresIn:
        ms(this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRE')) / 1000,
    });
  }

  async processRefreshToken(refreshToken: string, response: Response) {
    try {
      this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      });

      const user: IGetUser = await this.usersService.findUserByRefreshToken(
        refreshToken,
      );
      const userToken: IUser = {
        _id: user._id,
        email: user.email,
        name: user.name,
        role: {
          _id: user.role,
          name: '',
        },
      };

      return await this.handleToken(userToken, response);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async handleToken(user: IUser, response: Response) {
    const { _id, email, name, role } = user;
    const permissions = await this.getUserPermissions(_id);
    const payload: object = {
      sub: 'token login',
      iss: 'from server',
      _id,
      name,
      email,
      role,
    };
    const refresh_token: string = this.createRefreshToken(payload);
    await this.usersService.updateRefreshToken(refresh_token, _id);

    response.clearCookie('refresh_token');
    response.cookie('refresh_token', refresh_token, {
      maxAge: ms(this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRE')),
      httpOnly: true,
    });

    return {
      access_token: this.jwtService.sign(payload),
      user: {
        _id,
        name,
        email,
        role,
        permissions,
      },
    };
  }

  async logout(refreshToken: string, response: Response, user: IUser) {
    try {
      this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      });

      this.usersService.updateRefreshToken('', user._id);
      response.clearCookie('refresh_token');
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async getUserPermissions(userId: mongoose.Schema.Types.ObjectId) {
    return await this.usersService.getUserPermission(userId);
  }
}
