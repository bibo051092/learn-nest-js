import { IByUser } from './user';

export const arrayResumeStatus: string[] = [
  'PENDING',
  'REVIEWING',
  'APPROVED',
  'REJECTED',
];

export interface History {
  status: string;
  updatedAt: Date;
  updatedBy: IByUser;
}
