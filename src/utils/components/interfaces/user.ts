import mongoose, { Document } from 'mongoose';
import { IByCompany } from './company';

export interface IGetUser extends Document {
  name: string;
  email: string;
  password: string;
  age: number;
  gender: string;
  address: string;
  company: IByCompany;
  role: mongoose.Schema.Types.ObjectId;
  refreshToken: string;
  createdBy: IByUser;
  updatedBy: IByUser;
  deletedBy: IByUser;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  isDeleted: boolean;
}

export interface IUser {
  _id: mongoose.Schema.Types.ObjectId;
  name: string;
  email: string;
  role: {
    _id: mongoose.Schema.Types.ObjectId;
    name: string;
  };
  permissions?: {
    _id: mongoose.Schema.Types.ObjectId;
    name: string;
    apiPath: string;
    module: string;
  }[];
}

export interface IByUser {
  _id: mongoose.Schema.Types.ObjectId;
  email: string;
}
