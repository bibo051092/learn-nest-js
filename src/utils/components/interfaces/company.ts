import mongoose from 'mongoose';

export interface IByCompany {
  _id: mongoose.Schema.Types.ObjectId;
  name: string;
  logo: string;
}
