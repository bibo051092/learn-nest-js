import {
  IsDateString,
  IsMongoId,
  IsNotEmpty,
  IsNotEmptyObject,
  IsObject,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import mongoose from 'mongoose';

export class HistoryByUser {
  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  _id: mongoose.Schema.Types.ObjectId;

  @IsString()
  @IsNotEmpty()
  email: string;
}

export class History {
  @IsString()
  @IsNotEmpty()
  status: string;

  @IsDateString()
  @IsNotEmpty()
  updatedAt: Date;

  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => HistoryByUser)
  updatedBy: HistoryByUser;
}
