import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'noEmptyStringArray', async: false })
export class NoEmptyStringArrayConstraint
  implements ValidatorConstraintInterface
{
  validate(value: any, args: ValidationArguments) {
    if (!value || !Array.isArray(value) || value.length === 0) {
      return false;
    }

    return value.every(
      (item) => typeof item === 'string' && item.trim() !== '',
    );
  }

  defaultMessage(args: ValidationArguments) {
    return 'Each string in the array must not be empty.';
  }
}

export function NoEmptyStringArray(validationOptions?: ValidationOptions) {
  return function (object: Record<string, any>, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: NoEmptyStringArrayConstraint,
    });
  };
}
