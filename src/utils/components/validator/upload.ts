import { HttpStatus, ParseFilePipeBuilder } from '@nestjs/common';

export const handleValidateFile = () => {
  return new ParseFilePipeBuilder()
    .addFileTypeValidator({
      fileType: /\/(jpeg|png)$/, // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
    })
    .addMaxSizeValidator({
      maxSize: 1024 * 1024,
    })
    .build({
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
    });
};
