import mongoose from 'mongoose';
import { NotFoundException } from '@nestjs/common';
import aqp from 'api-query-params';

export const checkValidId = (id, message: string) => {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw new NotFoundException(message);
  }
};

export const handlePaginate = async (
  current: number,
  pageSize: number,
  queryString: string,
  model: any,
) => {
  const { filter, sort, population, projection } = aqp(queryString);
  const offset = (current - 1) * pageSize;
  const defaultLimit = pageSize ? pageSize : 10;

  delete filter.current;
  delete filter.pageSize;

  const totalItems = await model.countDocuments(filter);
  const totalPages = Math.ceil(totalItems / defaultLimit);
  const result = await model
    .find(filter)
    .skip(offset)
    .limit(defaultLimit)
    .sort(sort as any)
    .select(projection as any)
    .populate(population)
    .exec();

  return {
    meta: {
      current: current, //trang hiện tại
      pageSize: pageSize, //số lượng bản ghi đã lấy
      pages: totalPages, //tổng số trang với điều kiện query
      total: totalItems, // tổng số phần tử (số bản ghi)
    },
    result, //kết quả query
  };
};
