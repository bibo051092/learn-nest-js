import { BadRequestException } from '@nestjs/common';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { PermissionDocument } from '../../../permissions/schemas/permission.schema';

export const checkApiPathAndMethod = async (
  permissionModel: SoftDeleteModel<PermissionDocument>,
  apiPath: string,
  method: string,
) => {
  const isExist = await permissionModel.findOne({ apiPath, method });

  if (isExist) {
    throw new BadRequestException(
      `apiPath=${apiPath} and method=${method} is exist.`,
    );
  }
};
