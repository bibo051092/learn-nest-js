import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto, RegisterUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserDocument } from './schemas/user.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { getHashPassword, checkValidId, IUser, handlePaginate } from '../utils';
import mongoose from 'mongoose';
import { RolesService } from '../roles/roles.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private userModel: SoftDeleteModel<UserDocument>,
    private roleService: RolesService,
  ) {}

  async create(
    userDto: CreateUserDto | RegisterUserDto,
    currentUser: IUser | undefined,
  ) {
    const isExistUser = await this.findByEmail(userDto.email);
    if (isExistUser) {
      throw new BadRequestException('Email must be unique.');
    }

    const roleUserId = await this.roleService.getUserRoleId();
    const createdBy = currentUser
      ? {
          _id: currentUser._id,
          email: currentUser.email,
        }
      : {};
    const userCreated = await this.userModel.create({
      ...userDto,
      password: getHashPassword(userDto.password),
      role: roleUserId,
      createdBy,
    });

    return {
      _id: userCreated._id,
      createdAt: userCreated.createdAt,
    };
  }

  async findOne(id: mongoose.Schema.Types.ObjectId): Promise<User> {
    checkValidId(id, 'User not found!');
    return this.userModel
      .findOne({ _id: id })
      .select('-password')
      .populate({ path: 'role', select: { _id: 1, name: 1 } });
  }

  async findByEmail(email: string) {
    return this.userModel
      .findOne({ email })
      .populate({ path: 'role', select: { name: 1 } });
  }

  async update(updateUserDto: UpdateUserDto, user: IUser) {
    checkValidId(updateUserDto._id, 'User not found!');
    return this.userModel.updateOne(
      { _id: updateUserDto._id },
      {
        ...updateUserDto,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );
  }

  async remove(id: string, user: IUser) {
    checkValidId(id, 'User not found!');

    const userExist = await this.userModel.findOne({ _id: id });
    if (userExist.email === 'admin@gmail.com') {
      throw new BadRequestException(`Not delete user admin@gmail.com`);
    }

    await this.userModel.updateOne(
      { _id: id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return this.userModel.softDelete({
      _id: id,
    });
  }

  findAll(current: number, pageSize: number, queryString: string) {
    return handlePaginate(current, pageSize, queryString, this.userModel);
  }

  updateRefreshToken(
    refreshToken: string,
    _id: mongoose.Schema.Types.ObjectId,
  ) {
    return this.userModel.updateOne({ _id }, { refreshToken });
  }

  findUserByRefreshToken(refreshToken: string) {
    return this.userModel
      .findOne({ refreshToken })
      .populate({ path: 'role', select: { name: 1 } })
      .select('-password');
  }

  async getUserPermission(userId: mongoose.Schema.Types.ObjectId) {
    const user = await this.findOne(userId);
    const userRole = user.role as unknown as {
      _id: mongoose.Schema.Types.ObjectId;
      name: string;
    };
    const role: any = await this.roleService.getRoleById(userRole._id);
    return role.permissions;
  }
}
