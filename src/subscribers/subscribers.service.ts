import { Injectable } from '@nestjs/common';
import { CreateSubscriberDto } from './dto/create-subscriber.dto';
import { UpdateSubscriberDto } from './dto/update-subscriber.dto';
import { checkValidId, handlePaginate, IUser } from '../utils';
import { InjectModel } from '@nestjs/mongoose';
import { Subscriber, SubscriberDocument } from './schemas/subscriber.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import mongoose from 'mongoose';

@Injectable()
export class SubscribersService {
  constructor(
    @InjectModel(Subscriber.name)
    private subscriberModel: SoftDeleteModel<SubscriberDocument>,
  ) {}

  async create(createSubscriberDto: CreateSubscriberDto, user: IUser) {
    const subscriber = await this.subscriberModel.create({
      ...createSubscriberDto,
      createdBy: {
        _id: user._id,
        email: user.email,
      },
    });

    return {
      _id: subscriber._id,
      createdAt: subscriber.email,
    };
  }

  async update(
    _id: mongoose.Schema.Types.ObjectId,
    updateSubscriberDto: UpdateSubscriberDto,
    user: IUser,
  ) {
    checkValidId(_id, 'Subscriber not found');
    return this.subscriberModel.updateOne(
      { _id },
      {
        ...updateSubscriberDto,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );
  }

  getSubscribers(current, pageSize, queryString) {
    return handlePaginate(current, pageSize, queryString, this.subscriberModel);
  }

  getSubscriberById(_id: mongoose.Schema.Types.ObjectId) {
    checkValidId(_id, 'Subscriber not found');
    return this.subscriberModel.findOne({ _id });
  }

  async remove(_id: mongoose.Schema.Types.ObjectId, user: IUser) {
    checkValidId(_id, 'Subscriber not found');

    await this.subscriberModel.updateOne(
      { _id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return this.subscriberModel.softDelete({ _id });
  }
}
