import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { SubscribersService } from './subscribers.service';
import { CreateSubscriberDto } from './dto/create-subscriber.dto';
import { UpdateSubscriberDto } from './dto/update-subscriber.dto';
import { IUser, ResponseMessage, SkipCheckPermission, User } from '../utils';
import mongoose from 'mongoose';

@Controller('subscribers')
export class SubscribersController {
  constructor(private readonly subscribersService: SubscribersService) {}

  @ResponseMessage('Create new a subscriber')
  @Post()
  handleCreate(
    @Body() createSubscriberDto: CreateSubscriberDto,
    @User() user: IUser,
  ) {
    return this.subscribersService.create(createSubscriberDto, user);
  }

  @ResponseMessage('Update a subscriber')
  @SkipCheckPermission()
  @Patch(':id')
  handleUpdate(
    @Param('id') id: mongoose.Schema.Types.ObjectId,
    @Body() updateSubscriberDto: UpdateSubscriberDto,
    @User() user: IUser,
  ) {
    return this.subscribersService.update(id, updateSubscriberDto, user);
  }

  @ResponseMessage('Fetch list subscribers')
  @Get()
  handleGetSubscribers(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.subscribersService.getSubscribers(
      +current,
      +pageSize,
      queryString,
    );
  }

  @ResponseMessage('Fetch a subscriber')
  @Get(':id')
  handleGetSubscriberById(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    return this.subscribersService.getSubscriberById(id);
  }

  @ResponseMessage('Delete a subscriber')
  @Delete(':id')
  handleDelete(
    @Param('id') id: mongoose.Schema.Types.ObjectId,
    @User() user: IUser,
  ) {
    return this.subscribersService.remove(id, user);
  }
}
