import { PartialType } from '@nestjs/mapped-types';
import { CreateResumeDto } from './create-resume.dto';
import { IsIn, IsNotEmpty, IsString } from 'class-validator';
import { arrayResumeStatus } from '../../utils';

export class UpdateResumeDto extends PartialType(CreateResumeDto) {}

export class UpdateStatusResumeDto {
  @IsString()
  @IsNotEmpty()
  @IsIn(arrayResumeStatus)
  status: string;
}
