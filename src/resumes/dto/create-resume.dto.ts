import {
  ArrayNotEmpty,
  IsArray,
  IsEmail,
  IsIn,
  IsMongoId,
  IsNotEmpty,
  IsString,
  IsUrl,
  ValidateNested,
} from 'class-validator';
import mongoose from 'mongoose';
import { arrayResumeStatus, NoEmptyStringArray } from '../../utils';
import { Type } from 'class-transformer';
import { History } from '../../utils/components/classes/resume';

export class CreateResumeDto {
  @IsString()
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  userId: mongoose.Schema.Types.ObjectId;

  @IsString()
  @IsNotEmpty()
  @IsUrl()
  url: string;

  @IsString()
  @IsNotEmpty()
  @IsIn(arrayResumeStatus)
  status: string;

  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  company: mongoose.Schema.Types.ObjectId;

  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  job: mongoose.Schema.Types.ObjectId;

  @IsArray()
  @ArrayNotEmpty()
  @NoEmptyStringArray()
  @ValidateNested()
  @Type(() => History)
  history: History[];
}

export class CreateUserCvDto {
  @IsString()
  @IsNotEmpty()
  // @IsUrl()
  url: string;

  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  company: mongoose.Schema.Types.ObjectId;

  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  job: mongoose.Schema.Types.ObjectId;
}
