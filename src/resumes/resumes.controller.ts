import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ResumesService } from './resumes.service';
import { IUser, ResponseMessage, User } from '../utils';
import { CreateUserCvDto } from './dto/create-resume.dto';
import { UpdateStatusResumeDto } from './dto/update-resume.dto';

@Controller('resumes')
export class ResumesController {
  constructor(private readonly resumesService: ResumesService) {}

  @ResponseMessage('Create a new Resume')
  @Post()
  handleCreate(@Body() createResumeDto: CreateUserCvDto, @User() user: IUser) {
    return this.resumesService.create(createResumeDto, user);
  }

  @ResponseMessage('Fetch all resumes with paginate')
  @Get()
  handleGetResumes(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.resumesService.getResumes(+current, +pageSize, queryString);
  }

  @ResponseMessage('Fetch resume by id')
  @Get(':id')
  handleGetResumeById(@Param('id') id: string) {
    return this.resumesService.getResumeById(id);
  }

  @ResponseMessage('Update status resume')
  @Patch(':id')
  handleUpdateResume(
    @Param('id') id: string,
    @Body() updateStatusResumeDto: UpdateStatusResumeDto,
    @User() user: IUser,
  ) {
    return this.resumesService.updateStatusResume(
      id,
      updateStatusResumeDto,
      user,
    );
  }

  @ResponseMessage('Delete a resume by id')
  @Delete(':id')
  handleDeleteResume(@Param('id') id: string, @User() user: IUser) {
    return this.resumesService.removeResumeById(id, user);
  }

  @ResponseMessage('Get Resumes by User')
  @Post('by-user')
  handleGetResumeByUser(@User() user: IUser) {
    return this.resumesService.getResumeByUser(user);
  }
}
