import { Injectable } from '@nestjs/common';
import { checkValidId, handlePaginate, IUser } from '../utils';
import { CreateUserCvDto } from './dto/create-resume.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Resume, ResumeDocument } from './schemas/resume.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { UpdateStatusResumeDto } from './dto/update-resume.dto';

@Injectable()
export class ResumesService {
  constructor(
    @InjectModel(Resume.name)
    private resumeModel: SoftDeleteModel<ResumeDocument>,
  ) {}

  async create(createResumeDto: CreateUserCvDto, user: IUser) {
    const resume = await this.resumeModel.create({
      ...createResumeDto,
      email: user.email,
      userId: user._id,
      status: 'PENDING',
      history: [
        {
          status: 'PENDING',
          updatedAt: new Date(),
          updatedBy: {
            _id: user._id,
            email: user.email,
          },
        },
      ],
      createdBy: {
        _id: user._id,
        email: user.email,
      },
    });

    return {
      _id: resume._id,
      createdAt: resume.createdAt,
    };
  }

  getResumes(current: number, pageSize: number, queryString: string) {
    return handlePaginate(current, pageSize, queryString, this.resumeModel);
  }

  getResumeById(_id: string) {
    checkValidId(_id, 'Resume not found');
    return this.resumeModel.findOne({ _id });
  }

  async updateStatusResume(
    _id: string,
    updateStatusResumeDto: UpdateStatusResumeDto,
    user: IUser,
  ) {
    checkValidId(_id, 'Resume not found');
    const { status } = updateStatusResumeDto;

    return this.resumeModel.updateOne(
      { _id },
      {
        status,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
        $push: {
          history: {
            status,
            updatedAt: new Date(),
            updatedBy: {
              _id: user._id,
              email: user.email,
            },
          },
        },
      },
    );
  }

  async removeResumeById(_id: string, user: IUser) {
    checkValidId(_id, 'Resume not found');

    await this.resumeModel.updateOne(
      { _id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return this.resumeModel.softDelete({ _id });
  }

  getResumeByUser(user: IUser) {
    return this.resumeModel
      .find({ userId: user._id })
      .sort('-createdAt')
      .populate([
        {
          path: 'company',
          select: { name: 1 },
        },
        {
          path: 'job',
          select: { name: 1 },
        },
      ]);
  }
}
