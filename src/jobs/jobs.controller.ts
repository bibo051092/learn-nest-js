import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { JobsService } from './jobs.service';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { ResponseMessage, User, IUser, Public } from '../utils';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @ResponseMessage('Create a new job')
  @Post('/create')
  handleCreate(@Body() createJobDto: CreateJobDto, @User() user: IUser) {
    return this.jobsService.create(createJobDto, user);
  }

  @ResponseMessage('Update a job')
  @Patch(':id')
  handleUpdate(
    @Param('id') id: string,
    @Body() updateJobDto: UpdateJobDto,
    @User() user: IUser,
  ) {
    return this.jobsService.update(id, updateJobDto, user);
  }

  @ResponseMessage('Delete a job')
  @Delete(':id')
  handleDelete(@Param('id') id: string, @User() user: IUser) {
    return this.jobsService.remove(id, user);
  }

  @Public()
  @ResponseMessage('Fetch a job by id')
  @Get(':id')
  handleGetJobById(@Param('id') id: string) {
    return this.jobsService.getJobById(id);
  }

  @Public()
  @ResponseMessage('Fetch jobs with pagination')
  @Get()
  handleGetJobWithPagination(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.jobsService.getJobWithPagination(
      +current,
      +pageSize,
      queryString,
    );
  }
}
