import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Job, JobDocument } from './schemas/job.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { checkValidId, handlePaginate, IUser } from '../utils';

@Injectable()
export class JobsService {
  constructor(
    @InjectModel(Job.name)
    private jobModel: SoftDeleteModel<JobDocument>,
  ) {}

  async create(createJobDto: CreateJobDto, user: IUser) {
    const job = await this.jobModel.create({
      ...createJobDto,
      createdBy: {
        _id: user._id,
        email: user.email,
      },
    });

    return {
      _id: job._id,
      createdAt: job.createdAt,
    };
  }

  update(_id: string, updateJobDto: UpdateJobDto, user: IUser) {
    checkValidId(_id, 'Job not found');
    return this.jobModel.updateOne(
      { _id },
      {
        ...updateJobDto,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );
  }

  async remove(_id: string, user: IUser) {
    checkValidId(_id, 'Job not found');

    await this.jobModel.updateOne(
      { _id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return this.jobModel.softDelete({ _id });
  }

  getJobById(_id: string) {
    checkValidId(_id, 'Job not found');

    return this.jobModel.findOne({ _id });
  }

  getJobWithPagination(current: number, pageSize: number, queryString: string) {
    return handlePaginate(current, pageSize, queryString, this.jobModel);
  }
}
