import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { RolesService } from './roles.service';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { IUser, ResponseMessage, User } from '../utils';
import mongoose from 'mongoose';

@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @ResponseMessage('Create a new role')
  @Post()
  handleCreate(@Body() createRoleDto: CreateRoleDto, @User() user: IUser) {
    return this.rolesService.create(createRoleDto, user);
  }

  @ResponseMessage('Update a role')
  @Patch(':id')
  handleUpdate(
    @Param('id') id: string,
    updateRoleDto: UpdateRoleDto,
    @User() user: IUser,
  ) {
    return this.rolesService.update(id, updateRoleDto, user);
  }

  @ResponseMessage('Fetch Roles with paginate')
  @Get()
  handleGetRoles(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.rolesService.getRoles(+current, +pageSize, queryString);
  }

  @ResponseMessage('Fetch role by Id')
  @Get(':id')
  handleGetRoleById(@Param('id') id: mongoose.Schema.Types.ObjectId) {
    return this.rolesService.getRoleById(id);
  }

  @ResponseMessage('Delete a Role')
  @Delete(':id')
  handleDelete(@Param('id') id: string, @User() user: IUser) {
    return this.rolesService.remove(id, user);
  }
}
