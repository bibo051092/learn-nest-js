import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Role, RoleDocument } from './schemas/role.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { checkValidId, handlePaginate, IUser, USER_ROLE } from '../utils';
import mongoose from 'mongoose';

@Injectable()
export class RolesService {
  constructor(
    @InjectModel(Role.name)
    private roleModel: SoftDeleteModel<RoleDocument>,
  ) {}

  async create(createRoleDto: CreateRoleDto, user: IUser) {
    const isExist = await this.roleModel.findOne({ name: createRoleDto.name });
    if (isExist) {
      throw new BadRequestException(`name=${createRoleDto.name} is exist.`);
    }

    const role = await this.roleModel.create({
      ...createRoleDto,
      createdBy: {
        _id: user._id,
        email: user.email,
      },
    });

    return {
      _id: role._id,
      createdAt: role.createdAt,
    };
  }

  async update(_id: string, updateRoleDto: UpdateRoleDto, user: IUser) {
    checkValidId(_id, 'Role not found');

    // const isExist = await this.roleModel.findOne({ name: updateRoleDto.name });
    // if (isExist) {
    //   throw new BadRequestException(`name=${updateRoleDto.name} is exist.`);
    // }

    return this.roleModel.updateOne(
      { _id },
      {
        ...updateRoleDto,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );
  }

  async getRoles(current: number, pageSize: number, queryString: string) {
    return await handlePaginate(current, pageSize, queryString, this.roleModel);
  }

  async getRoleById(_id: mongoose.Schema.Types.ObjectId) {
    checkValidId(_id, 'Role not found');
    return this.roleModel.findOne({ _id }).populate({
      path: 'permissions',
      select: { _id: 1, apiPath: 1, name: 1, method: 1, module: 1 },
    });
  }

  async remove(_id: string, user: IUser) {
    checkValidId(_id, 'Role not found');

    const roleExist = await this.roleModel.findOne({ _id });
    if (roleExist.name === 'ADMIN') {
      throw new BadRequestException(`Not delete role ADMIN`);
    }

    await this.roleModel.updateOne(
      { _id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return await this.roleModel.softDelete({ _id });
  }

  getUserRoleId() {
    return this.roleModel.findOne({ name: USER_ROLE }).select('_id');
  }
}
