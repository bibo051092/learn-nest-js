import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { CreateCompanyDto } from './dto/create-company.dto';
import { IUser, Public, ResponseMessage, User } from '../utils';
import { UpdateCompanyDto } from './dto/update-company.dto';

@Controller('companies')
export class CompaniesController {
  constructor(private readonly companiesService: CompaniesService) {}

  @ResponseMessage('Create company by id')
  @Post()
  handleCreate(
    @Body() createCompanyDto: CreateCompanyDto,
    @User() user: IUser,
  ) {
    return this.companiesService.create(createCompanyDto, user);
  }

  @ResponseMessage('Update company by id')
  @Patch(':id')
  handleUpdate(
    @Param('id') id: string,
    @Body() updateCompanyDto: UpdateCompanyDto,
    @User() user: IUser,
  ) {
    return this.companiesService.update(id, updateCompanyDto, user);
  }

  @ResponseMessage('Delete company by id')
  @Delete(':id')
  handleDelete(@Param('id') id: string, @User() user: IUser) {
    return this.companiesService.remove(id, user);
  }

  @Public()
  @ResponseMessage('Fetch data list company')
  @Get()
  handleGetCompanies(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.companiesService.findAll(+current, +pageSize, queryString);
  }

  @Public()
  @ResponseMessage('Fetch company by id')
  @Get(':id')
  handleGetCompanyById(@Param('id') id: string) {
    return this.companiesService.findById(id);
  }
}
