import { Injectable } from '@nestjs/common';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Permission, PermissionDocument } from './schemas/permission.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import {
  checkValidId,
  IUser,
  checkApiPathAndMethod,
  handlePaginate,
} from '../utils';

@Injectable()
export class PermissionsService {
  constructor(
    @InjectModel(Permission.name)
    private permissionModel: SoftDeleteModel<PermissionDocument>,
  ) {}

  async create(createPermissionDto: CreatePermissionDto, user: IUser) {
    const { name, apiPath, method, module } = createPermissionDto;
    const { _id, email } = user;

    await checkApiPathAndMethod(this.permissionModel, apiPath, method);

    const permission = await this.permissionModel.create({
      name,
      apiPath,
      method,
      module,
      createdBy: { _id, email },
    });

    return {
      _id: permission._id,
      createdAt: permission.createdAt,
    };
  }

  async update(
    _id: string,
    updatePermissionDto: UpdatePermissionDto,
    user: IUser,
  ) {
    checkValidId(_id, 'Permission not found!');
    await checkApiPathAndMethod(
      this.permissionModel,
      updatePermissionDto.apiPath,
      updatePermissionDto.method,
    );

    return this.permissionModel.updateOne(
      { _id },
      {
        ...updatePermissionDto,
        updatedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );
  }

  getPermissions(current: number, pageSize: number, queryString: string) {
    return handlePaginate(current, pageSize, queryString, this.permissionModel);
  }

  getPermissionById(_id: string) {
    checkValidId(_id, 'Permission not found!');
    return this.permissionModel.findOne({ _id });
  }

  async remove(_id: string, user: IUser) {
    checkValidId(_id, 'Permission not found!');

    await this.permissionModel.updateOne(
      { _id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email,
        },
      },
    );

    return this.permissionModel.softDelete({ _id });
  }
}
