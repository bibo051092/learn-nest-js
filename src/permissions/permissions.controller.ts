import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { PermissionsService } from './permissions.service';
import { CreatePermissionDto } from './dto/create-permission.dto';
import { UpdatePermissionDto } from './dto/update-permission.dto';
import { IUser, ResponseMessage, User } from '../utils';

@Controller('permissions')
export class PermissionsController {
  constructor(private readonly permissionsService: PermissionsService) {}

  @ResponseMessage('Create a new permission')
  @Post()
  handleCreate(
    @Body() createPermissionDto: CreatePermissionDto,
    @User() user: IUser,
  ) {
    return this.permissionsService.create(createPermissionDto, user);
  }

  @ResponseMessage('Update a permission')
  @Patch(':id')
  handleUpdate(
    @Param('id') id: string,
    @Body() updatePermissionDto: UpdatePermissionDto,
    @User() user: IUser,
  ) {
    return this.permissionsService.update(id, updatePermissionDto, user);
  }

  @ResponseMessage('Fetch permissions with paginate')
  @Get()
  handleGetPermissions(
    @Query('current') current: string,
    @Query('pageSize') pageSize: string,
    @Query() queryString: string,
  ) {
    return this.permissionsService.getPermissions(
      +current,
      +pageSize,
      queryString,
    );
  }

  @ResponseMessage('Fetch a permission by id')
  @Get(':id')
  handleGetPermissionById(@Param('id') id: string) {
    return this.permissionsService.getPermissionById(id);
  }

  @ResponseMessage('Delete a permission')
  @Delete(':id')
  handleDelete(@Param('id') id: string, @User() user: IUser) {
    return this.permissionsService.remove(id, user);
  }
}
