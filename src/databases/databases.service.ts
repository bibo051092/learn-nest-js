import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../users/schemas/user.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import {
  Permission,
  PermissionDocument,
} from '../permissions/schemas/permission.schema';
import { Role, RoleDocument } from '../roles/schemas/role.schema';
import { ConfigService } from '@nestjs/config';
import {
  ADMIN_ROLE,
  getHashPassword,
  INIT_PERMISSIONS,
  USER_ROLE,
} from '../utils';

@Injectable()
export class DatabasesService implements OnModuleInit {
  private readonly logger = new Logger(DatabasesService.name);

  constructor(
    @InjectModel(Permission.name)
    private permissionModel: SoftDeleteModel<PermissionDocument>,
    @InjectModel(Role.name)
    private roleModel: SoftDeleteModel<RoleDocument>,
    @InjectModel(User.name)
    private userModel: SoftDeleteModel<UserDocument>,

    private configService: ConfigService,
  ) {}

  async onModuleInit(): Promise<any> {
    const isInstallSampleData = Boolean(
      this.configService.get<string>('SHOULD_INIT'),
    );
    if (isInstallSampleData) {
      const countPermissions = await this.permissionModel.count({});
      const countRoles = await this.roleModel.count({});
      const countUsers = await this.userModel.count({});

      if (countPermissions === 0) {
        await this.permissionModel.insertMany(INIT_PERMISSIONS);
      }

      if (countRoles === 0) {
        const permissions = await this.permissionModel.find({}).select('_id');

        await this.roleModel.insertMany([
          {
            name: ADMIN_ROLE,
            description: 'ADMIN ROLE',
            isActive: true,
            permissions: permissions,
          },
          {
            name: USER_ROLE,
            description: 'USER ROLE',
            isActive: true,
            permissions: [],
          },
        ]);
      }

      if (countUsers === 0) {
        const roleAdmin = await this.roleModel.findOne({ name: ADMIN_ROLE });
        const roleUser = await this.roleModel.findOne({ name: USER_ROLE });

        await this.userModel.insertMany([
          {
            name: 'ADMIN',
            email: 'admin@gmail.com',
            password: getHashPassword(
              this.configService.get<string>('INIT_PASSWORD'),
            ),
            age: '18',
            gender: 1,
            address: '123 Abcdef',
            role: roleAdmin?._id,
            refreshToken: '',
            company: {
              _id: '64871701c7573fac797f83ea',
              name: 'Amazon.com, Inc',
            },
          },
          {
            name: 'USER',
            email: 'user@gmail.com',
            password: getHashPassword(
              this.configService.get<string>('INIT_PASSWORD'),
            ),
            age: '18',
            gender: 1,
            address: '123 Abcdef',
            role: roleUser?._id,
            refreshToken: '',
            company: {
              _id: '64871701c7573fac797f83ea',
              name: 'Amazon.com, Inc',
            },
          },
        ]);
      }

      if (countPermissions > 0 && countRoles > 0 && countUsers > 0) {
        this.logger.log('ALREADY SAMPLE DATA');
      }
    }
  }
}
