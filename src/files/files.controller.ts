import {
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FilesService } from './files.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { handleValidateFile, ResponseMessage } from '../utils';
import { NestInterceptor } from '@nestjs/common/interfaces';

@Controller('files')
export class FilesController {
  constructor(private readonly filesService: FilesService) {}

  @ResponseMessage('Upload Single File')
  @Post('upload')
  @UseInterceptors(FileInterceptor('fileUpload') as NestInterceptor | Function) //tên field sử dụng trong form-data
  uploadFile(@UploadedFile(handleValidateFile()) file: Express.Multer.File) {
    return {
      fileName: file.filename,
    };
  }
}
