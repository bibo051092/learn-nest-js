import { BadRequestException, Injectable } from '@nestjs/common';
import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import fs from 'fs/promises';
import { diskStorage } from 'multer';
import * as path from 'path';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  getRootPath = () => process.cwd();

  async ensureExists(targetDirectory: string) {
    try {
      // Directory successfully created, or it already exists.
      await fs.mkdir(targetDirectory, { recursive: true });
    } catch (error) {
      if (error.code === 'EEXIST') {
        throw new BadRequestException(
          "Requested location already exists, but it's not a directory.",
        );
      } else if (error.code === 'ENOTDIR') {
        throw new BadRequestException(
          "The parent hierarchy contains a file with the same name as the dir you're trying to create.",
        );
      } else {
        throw new BadRequestException(
          'Some other error like permission denied.',
        );
      }
    }
  }

  createMulterOptions(): MulterModuleOptions {
    return {
      storage: diskStorage({
        destination: async (req, file, cb) => {
          const folder = req?.headers?.folder_type ?? 'common';
          const targetPath = path.join(
            this.getRootPath(),
            `public/assets/images/${folder}`,
          );
          await this.ensureExists(targetPath);
          cb(null, targetPath);
        },
        filename: (req, file, cb) => {
          //get image extension
          const extName = path.extname(file.originalname);
          //get image's name (without extension)
          const baseName = path.basename(file.originalname, extName);
          const finalName = `${baseName}-${Date.now()}${extName}`;
          cb(null, finalName);
        },
      }),
    };
  }
}
