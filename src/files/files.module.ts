import { Module } from '@nestjs/common';
import { FilesService } from './files.service';
import { FilesController } from './files.controller';
import {
  MulterModule,
  MulterModuleAsyncOptions,
} from '@nestjs/platform-express';
import { MulterConfigService } from './files.config';

@Module({
  imports: [
    MulterModule.registerAsync({
      useClass: MulterConfigService,
    } as MulterModuleAsyncOptions),
  ],
  controllers: [FilesController],
  providers: [FilesService],
})
export class FilesModule {}
