import { Controller, Get } from '@nestjs/common';
import { MailService } from './mail.service';
import { Public, ResponseMessage } from '../utils';
import { MailerService } from '@nestjs-modules/mailer';
import { Cron, CronExpression } from '@nestjs/schedule';

@Controller('mail')
export class MailController {
  constructor(
    private readonly mailService: MailService,
    private readonly mailerService: MailerService,
  ) {}

  @Public()
  @ResponseMessage('Test email')
  @Cron(CronExpression.EVERY_DAY_AT_8AM)
  @Get()
  async handleSendEmail(
    toEmail: string,
    from: string,
    subject: string,
    template: string,
    context: object
  ) {
    await this.mailerService.sendMail({
      to: toEmail,
      from: from,
      subject: subject,
      template: template,
      context: context,
    });
  }
}
